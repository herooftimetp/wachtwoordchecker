wachtwoord = input(("vul een wachtwoord in "))
if len(wachtwoord) < 8:
    print("Het wachtwoord moet tussen de 8 en 128 karakters bevatten")
elif len(wachtwoord) > 128:
    print("Het wachtwoord moet tussen de 8 en 128 karakters bevatten")
elif wachtwoord.islower() == True:
    print("Het wachtwoord moet minstens 1 hoofdletter bevatten")
elif wachtwoord.isalnum() == True:
    print("Het wachtwoord moet minimaal 1 speciaal karakter bevatten bijvoorbeeld !@#$%")
elif not any(c.isdigit() for c in wachtwoord):
    print("Je wachtwoord moet nummers bevatten")
else:
    print("Je wachtwoord voldoet aan de eisen gefeliciteerd!")
